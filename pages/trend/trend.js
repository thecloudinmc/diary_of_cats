// pages/trend/trend.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    trends: [{
      id: 1,
      icon:'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3175633703,3855171020&fm=27&gp=0.jpg',
      username: "宣志强",
      content: '测试内容',
      useraddress: "山东财经大学",
      trendTime: "2018-07-25 10:14:23",
      zanSource: ['张三', '李四', '王五', '找钱', '孙俪', '王宝'],
      zanNum: 3,
      contnet: [{
        'firstname': '张三',
        'content': '你好漂亮呀！！'
      },
      {
        'firstname': '李四',
        'content': '纳尼！！'
      },
      {
        'firstname': '王五',
        'content': '鬼扯咧'
      }
      ]
    }
    ],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (e) {
    const that = this
    wx.request({
      url: 'http://localhost:8080/',
      success: function (res) {
        console.log(res.data)
        that.setData({

        })

      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})