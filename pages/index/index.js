//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    //轮播图
    imgUrls: [
      "/images/1.jpg", "/images/2.jpg", "/images/3.jpg"
    ],
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    trends: [{
        id: 1,
        icon: 'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3175633703,3855171020&fm=27&gp=0.jpg',
        username: "宣志强",
        content: '测试内容',
        useraddress: "山东财经大学",
        trendTime: "2018-07-25 10:14:23",
        zanSource: ['张三', '李四', '王五', '找钱', '孙俪', '王宝'],
        zanNum: 3,
        contnet: [{
            'firstname': '张三',
            'content': '你好漂亮呀！！'
          },
          {
            'firstname': '李四',
            'content': '纳尼！！'
          },
          {
            'firstname': '王五',
            'content': '鬼扯咧'
          }
        ]
      }
    ],
    photoWidth: wx.getSystemInfoSync().windowWidth / 5,
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(e) {
    const that = this
    wx.request({
      url: 'http://localhost:8080/',
      success: function(res) {
        console.log(res.data)
        that.setData({

        })

      }
    })
  },
  // 点击图片进行大图查看
  LookPhoto: function(e) {
    wx.previewImage({
      current: e.currentTarget.dataset.photurl,
      urls: this.data.resource,
    })
  },

  // 点击点赞的人
  TouchZanUser: function(e) {
    wx.showModal({
      title: e.currentTarget.dataset.name,
      showCancel: false
    })
  },

  // 删除朋友圈
  delete: function() {
    wx.showToast({
      title: '删除成功',
    })
  },

  
})