// pages/column/column.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    likeState: false,
    columnTitle: "我家猫那点事",
    columnUser: "小刀",
    columnContent:"今天，我家猫又又又又打碎东西了，每次都拿无辜的小眼神看我。。。我能怎么办。。。。。",
    columnTime: "9.26",
    hotColumns: [
      {
        id:1,
        hcImg: "/utils/images/mao1.jpg",
        hcTitle: "养猫到底有什么好",
        hcViews: "21.3k",
        hcComments: "63"
      },
      {
        id: 2,
        hcImg: "/utils/images/mao2.jpg",
        hcTitle: "哪个瞬间觉得你的猫爱上了你",
        hcViews: "6.5k",
        hcComments: "33"
      }
    ],
    comments: [
      {
        id: 1,
        commenterImg: "/utils/images/mao5.jpg",
        commentUser: "咪哆哆",
        commentContent: "猫咪这么可爱，应该多加点胡椒粉",
        commentState: false
      }
    ]
  },

  cancelLike: function() {
    this.setData({
      likeState: false
    })
  },

  goLike: function() {
    this.setData({
      likeState: true
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (e) {
    const that = this
    wx.request({
      url: 'http://localhost:8080/',
      success: function (res) {
        console.log(res.data)
        that.setData({

        })

      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})