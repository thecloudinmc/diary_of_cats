Page({
  onLoad: function (e) {
    const that = this
    wx.request({
      url: 'http://localhost:8080/',
      success: function (res) {
        console.log(res.data)
        that.setData({

        })

      }
    })
  },
  data: {
    array: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13','14','15','16','17','18','19','20'],
    objectArray: [
      {
        id: 0,
        name: '1'
      },
      {
        id: 1,
        name: '2'
      },
      {
        id: 2,
        name: '3'
      },
      {
        id: 3,
        name: '4'
      },
      {
        id: 4,
        name: '5'
      },
      {
        id: 5,
        name: '6'
      },
      {
        id: 6,
        name: '7'
      },
      {
        id: 7,
        name: '8'
      },
      {
        id: 8,
        name: '9'
      },
      {
        id: 9,
        name: '10'
      },
      {
        id: 10,
        name: '11'
      },
      {
        id: 11,
        name: '12'
      },
      {
        id: 12,
        name: '13'
      },
      {
        id: 13,
        name: '14'
      },
      {
        id: 14,
        name: '15'
      },
      {
        id: 15,
        name: '16'
      },
      {
        id: 16,
        name: '17'
      },
      {
        id: 17,
        name: '18'
      },
      {
        id: 18,
        name: '19'
      },
      {
        id: 19,
        name: '20'
      }
    ],
    index: 0,
    catlog: [
      {
        id: 1,
        headImg: "/images/4.jpeg",
        species: "ceshi",
        age: "3",
        description: "求领养"
      },
      {
        id: 2,
        headImg: "/images/5.jpeg",
        species: "ceshi",
        age: "3",
        description: "求领养"
      }
    ]
  },
    bindKeyInput: function (e) {
    this.setData({
      inputValue: e.detail.value
    })
  },
  bindReplaceInput: function (e) {
    var value = e.detail.value
    var pos = e.detail.cursor
    var left
    if (pos !== -1) {
      // 光标在中间
      left = e.detail.value.slice(0, pos)
      // 计算光标的位置
      pos = left.replace(/11/g, '2').length
    }

    // 直接返回对象，可以对输入进行过滤处理，同时可以控制光标的位置
    return {
      value: value.replace(/11/g, '2'),
      cursor: pos
    }

    // 或者直接返回字符串,光标在最后边
    // return value.replace(/11/g,'2'),
  },
  bindHideKeyboard: function (e) {
    if (e.detail.value === '123') {
      // 收起键盘
      wx.hideKeyboard()
    }
  },
  bindPickerChange: function (e) {
    this.setData({
      index: e.detail.value
    })
  },
})