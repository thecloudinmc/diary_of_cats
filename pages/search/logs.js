//logs.js
Page({
  data: {
    logs: []
  },
  trend: function () {
    wx.navigateTo({
      url: '../trend/trend'
    })
  },
  column: function () {
    wx.navigateTo({
      url: '../column/column'
    })
  },
  map: function () {
    wx.navigateTo({
      url: '../map/map'
    })
  },
  onLoad: function (e) {
    const that = this
    wx.request({
      url: 'http://localhost:8080/',
      success: function (res) {
        console.log(res.data)
        that.setData({

        })

      }
    })
  },
})
